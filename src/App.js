import React, {Component} from 'react';
import './App.less';
import PlayingPanel from "./PlayingPanel";
import ControlPanel from "./ControlPanel";


class App extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      answer: '',
      guessAnswer: ''
    };
    this.setAnswer = this.setAnswer.bind(this);
    this.setGuessAnswer = this.setGuessAnswer.bind(this);
    this.resetGuessAnswer = this.resetGuessAnswer.bind(this);
  }

  render() {
    return (
      <div className={'gameCard'}>
        <PlayingPanel answer={this.state.answer} guessAnswer={this.state.guessAnswer}
                      setGuessAnswer={this.setGuessAnswer}/>
        <ControlPanel setAnswer={this.setAnswer} resetGuessAnswer={this.resetGuessAnswer}/>
      </div>
    )
  }

  setAnswer(answer) {
    this.setState({
      answer: answer
    });
  }

  setGuessAnswer(inputValue) {
    this.setState({
      guessAnswer: inputValue
    });
  }

  resetGuessAnswer() {
    this.setGuessAnswer('');
  }
}

export default App;