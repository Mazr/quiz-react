import React, {Component} from 'react';
import './PlayingPanel.less';
import Result from "./Result";
import Guess from "./Guess";

class PlayingPanel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showGuessResult: false,
      isTrue: false
    };
    this.setGuessAnswer = this.setGuessAnswer.bind(this);
    this.setGuessResult = this.setGuessResult.bind(this);

  }

  render() {
    return (
      <div className={'playing-panel'}>

        <Result guessAnswer={this.props.guessAnswer} answer={this.props.answer}
                showGuessResult={this.state.showGuessResult} isTrue={this.state.isTrue}/>
        <Guess setGuessAnswer={this.setGuessAnswer} setGuessResult={this.setGuessResult} guessAnswer={this.props.guessAnswer}/>
      </div>
    )
  }

  setGuessResult(flag) {
    this.setState({
      showGuessResult: flag
    });
    if (this.props.answer === this.props.guessAnswer) {
      this.setState({
        isTrue: true
      })
    } else {
      this.setState({
        isTrue: false
      })
    }
  }

  setGuessAnswer(inputValue) {
    this.props.setGuessAnswer(inputValue);
    this.setState({
      showGuessResult: false
    });

  }
}

export default PlayingPanel;