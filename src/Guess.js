import React, {Component} from 'react';
import './Guess.less'

class Guess extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      submit: false,
      inputValue: ''
    };
    this.submitGuess = this.submitGuess.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
  }

  render() {
    return (
      <div className={'guess'} id={'guess-form'}>
        <form onSubmit={this.submitGuess}>
          <input maxLength={4} onChange={this.setInputValue} value={this.props.guessAnswer}/>
          <br/>
          <button className={'submit-button'} type={'submit'}>Guess</button>
        </form>
      </div>
    )
  }

  setInputValue(e) {
    this.props.setGuessAnswer(e.target.value)
  }

  submitGuess(e) {
    e.preventDefault();
    return this.props.setGuessResult(true);
  }
}

export default Guess;