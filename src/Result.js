import React, {Component} from 'react';
import './Result.less'

class Result extends Component {
  constructor(props, context) {
    super(props, context);
    this.getGameResult = this.getGameResult.bind(this);
    this.getLetterByIndex = this.getLetterByIndex.bind(this)
    this.state = {
      isTrue : false
    }
  }

  render() {
    return (
      <div className={'guess-result'}>
        <p className={'title'}>Your Result :</p>
        <span>{this.getLetterByIndex(0)}</span>
        <span>{this.getLetterByIndex(1)}</span>
        <span>{this.getLetterByIndex(2)}</span>
        <span>{this.getLetterByIndex(3)}</span>
        <p style={{color: this.props.isTrue? 'green' : 'red'}}>{this.getGameResult()}</p>
      </div>
    )
  }

  getLetterByIndex(index) {
    let guessAnswer = this.props.guessAnswer;
    if (guessAnswer.length > index) {
      return guessAnswer.charAt(index);
    }
  }

  getGameResult() {
    if(this.props.showGuessResult && this.props.guessAnswer !== '')
      return this.props.isTrue ? 'SUCCESS' : 'FAILED';
    return '';
  }
}

export default Result;