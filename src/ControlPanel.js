import React, {Component} from 'react';
import './ControlPanel.less';

class ControlPanel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      answer: '',
      showAnswer: true
    };
    this.getAnswer = this.getAnswer.bind(this);
    this.getAnswerFieldset = this.getAnswerFieldset.bind(this);
    this.showResult = this.showResult.bind(this);
  }

  render() {
    return (
      <div className={'control-panel'}>
        <button className={'new-card'} onClick={this.getAnswer}>New Card</button>
        {this.getAnswerFieldset()}
        <button className={'show-result'} onClick={this.showResult}>Show Result</button>
      </div>
    )
  }

  getAnswer() {
    this.props.resetGuessAnswer();
    this.setState({
      showAnswer: true
    });
    let result = ControlPanel.creatAnswer();
    this.setState({
      answer: result
    }, () => this.props.setAnswer(this.state.answer));
    setTimeout(() => {
      this.setState({
        showAnswer: false
      })
    }, 3000)
  }

  static creatAnswer() {
    let result = [];
    for (let i = 0; i < 4; i++) {
      const ranNum = Math.ceil(Math.random() * 25);
      result.push(String.fromCharCode(65 + ranNum));
    }
    return result.join('');
  }

  getAnswerFieldset() {
    if (this.state.showAnswer) {
      return (
        <div>
          <span>{this.state.answer.charAt(0)}</span>
          <span>{this.state.answer.charAt(1)}</span>
          <span>{this.state.answer.charAt(2)}</span>
          <span>{this.state.answer.charAt(3)}</span>
        </div>
      )
    } else {
      return (
        <div>
          <span>{''}</span>
          <span>{''}</span>
          <span>{''}</span>
          <span>{''}</span>
        </div>
      )
    }
  }

  showResult() {
    this.setState({
      showAnswer: true
    });
    this.props.resetGuessAnswer();
  }
}

export default ControlPanel;